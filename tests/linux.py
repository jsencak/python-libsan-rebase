#!/usr/bin/python

# Copyright (C) 2016 Red Hat, Inc.
# This file is part of libsan.
#
# libsan is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libsan is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libsan.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import, division, print_function, unicode_literals
import libsan.host.linux as linux
import libsan.host.scsi_debug as scsi_debug


def test_os_info():
    os_info = linux.query_os_info()
    if not os_info:
        print("FAIL: Could not query OS info")
        assert 0

    for info in os_info:
        print("%s: %s" % (info, os_info[info]))
    assert 1


def test_using_scsi_debug():
    if linux.is_docker():
        print("SKIP: Cannot create scsi_debug in container")
        return
    if not scsi_debug.scsi_debug_load_module():
        print("FAIL: loading scsi_debug module")
        assert 0

    device = scsi_debug.get_scsi_debug_devices()[-1]

    if linux.get_parent_device(device) != device:  # scsi_debug does not have parent
        print("FAIL: get_parent_device using scsi_debug")
        assert 0
    if linux.get_full_path(device) != '/dev/%s' % device:
        print("FAIL: get_full_path using scsi_debug")
        assert 0
    wwid = linux.get_device_wwid(device)
    if not wwid:
        print("FAIL: get_device_wwid using scsi_debug")
        assert 0
    if linux.get_udev_property(device, 'ID_MODEL') != 'scsi_debug':
        print("FAIL: device ID_MODEL should be 'scsi_debug'")
        assert 0
    if linux.is_dm_device(device):
        print("FAIL: scsi_debug device should not be mapped by DM")
        assert 0

    if not scsi_debug.scsi_debug_unload_module():
        print("FAIL: loading scsi_debug module")
        assert 0
    assert 1


def test_get_boot_device():
    get_boot = linux.get_boot_device()
    if get_boot is None and linux.is_docker() is False:
        print("FAIL: Could not find '/boot' and '/'! ")
        assert 0
    print("INFO: Boot device is: %s" % get_boot)
    assert 1


def test_get_dist_release():
    release = linux.dist_release()
    if not release:
        print("FAIL: Could not find distribution release")
        assert 0


def test_get_dist_ver():
    version = linux.dist_ver()
    if not version:
        print("FAIL: Could not find distribution version")
        assert 0


def test_get_dist_name():
    name = linux.dist_name()
    if not name:
        print("FAIL: Could not find distribution name")
        assert 0


def test_rpm_repo():
    repo_metalink = 'https://mirrors.fedoraproject.org/metalink?repo=fedora-debug-$releasever&arch=$basearch'
    repo_url = 'https://gitlab.com/mhoyer/python-libsan/-/raw/master/tests/test.repo'
    repo_name = 'testrepo'
    linux.add_repo('test1', repo_metalink, metalink=True)
    if not linux.check_repo('test1'):
        print("FAIL: Created repo is not working")
        assert 0
    if not linux.del_repo('test1'):
        print("FAIL: Unable to delete repo")
        assert 0
    linux.download_repo_file(repo_url)
    if not linux.check_repo(repo_name):
        print("FAIL: Test repo is not working")
        assert 0
    if not linux.del_repo('test1'):
        print("FAIL: Unable to delete repo")
        assert 0
