#!/usr/bin/python

# Copyright (C) 2021 Red Hat, Inc.
# This file is part of libsan.
#
# libsan is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libsan is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libsan.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import, division, print_function, unicode_literals

import unittest

import libsan.host.net as net

MAC = '7E:CD:60:1E:AC:6E'
NOTMAC = '7E:CD:60:1E:AC'
BADMAC = '7ecd.601e.ac6e'
LOWMAC = '7e:cd:60:1e:ac:6e'
NOTVALID = 'ThisIsNotValid'
LOCALHOST = '127.0.0.1'
NETMASK24 = '255.255.255.0'
CIDR24 = '24'
NOTIP = '256.256.256.256'
IPV6 = 'fd6f:60d2:2d9:0:b7e:b6e6:7bf5:c17e'


class TestNet(unittest.TestCase):

    def test_is_mac(self):
        self.assertTrue(net.is_mac(MAC))
        self.assertFalse(net.is_mac(NOTMAC))

    def test_get_nics(self):
        self.assertIsNotNone(net.get_nics())

    def test_get_mac_of_nics(self):
        self.assertIsNone(net.get_mac_of_nic(NOTVALID))
        self.assertIsNotNone(net.get_mac_of_nic(net.get_nics()[-1]))

    def test_get_nic_of_mac(self):
        self.assertIsNone(net.get_nic_of_mac(MAC))
        self.assertIsNotNone(net.get_nic_of_mac(net.get_mac_of_nic(net.get_nics()[-1])))

    def test_get_ip_address_of_nic(self):
        self.assertIsNone(net.get_ip_address_of_nic(NOTVALID))
        self.assertIsNotNone(net.get_ip_address_of_nic(net.get_nics()[-1]))

    def test_get_ipv6_address_of_nic(self):
        self.assertIsNone(net.get_ipv6_address_of_nic(NOTVALID))
        self.assertIsNotNone(net.get_ipv6_address_of_nic(net.get_nics()[-1]))

    def test_get_nic_of_ip(self):
        self.assertIsNone(net.get_nic_of_ip(NOTIP))
        self.assertIsNotNone(net.get_nic_of_ip(LOCALHOST))

    def test_driver_of_nic(self):
        self.assertIsNone(net.driver_of_nic(NOTVALID))

    def test_get_ip_version(self):
        self.assertEqual(4, net.get_ip_version(LOCALHOST))
        self.assertEqual(6, net.get_ip_version(IPV6))
        self.assertIsNone(net.get_ip_version(NOTIP))

    def test_standardize_mac(self):
        self.assertEqual(LOWMAC, net.standardize_mac(BADMAC))
        self.assertIsNone(net.standardize_mac(NOTMAC))

    def test_convert_netmask(self):
        self.assertEqual(int(CIDR24), net.convert_netmask(NETMASK24))
        self.assertEqual(int(CIDR24), net.convert_netmask(CIDR24))
        self.assertIsNone(net.convert_netmask(NOTVALID))

    def test_nm_get_conn(self):
        self.assertIsNone(net.nm_get_conn(MAC))
        self.assertIsNone(net.nm_get_conn(NOTVALID))

    def test_nm_get_conn_iface(self):
        self.assertIsNone(net.nm_get_conn_iface(NOTVALID))

    def test_nm_get_conn_uuid(self):
        self.assertIsNone(net.nm_get_conn_uuid(NOTVALID))

    def test_nm_get_conn_from_dev(self):
        self.assertIsNone(net.nm_get_conn_from_dev(NOTVALID))

    def test_nm_get_dev_from_conn(self):
        self.assertIsNone(net.nm_get_dev_from_conn(NOTVALID))

    def test_nm_conn_up(self):
        self.assertFalse(net.nm_conn_up(NOTVALID))

    def test_nm_set_ip(self):
        self.assertFalse(net.nm_set_ip(NOTVALID, NOTIP))
        self.assertFalse(net.nm_set_ip(NOTVALID, IPV6))
