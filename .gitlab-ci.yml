---
image: fedora:latest

stages:
  - Code Analysis
  - Test
  - Build
  - Deploy

linting:
  stage: Code Analysis
  script:
    # Need to install PIP first
    - dnf install -y python3-pip python3-netifaces python3-augeas python3-wheel
    # Install python syntax check tools plus dependencies
    - pip3 install flake8 pylint ssh2-python ipaddress distro six future python-augeas requests
    # We should enable Errors, Warnings and Flake errors once they are fixed
    - flake8 --exclude .git --max-line-length=120 .
    # Run pylint for general errors in code
    # W0105(pointless-string-statement) is disabled because we are using it for documentation
    # W0511(fixme) spots all fixme and todo comments
    # W0603(global-statement) disabled for now as we are using this for paths, move to some immutable config in the future
    # W0703(broad-except) sometimes we are catching general exception, because we are lazy. Lets stay lazy :)
    - pylint --disable C,R,W0105,W0511,W0603,W0703,W0705 --max-line-length=120 --msg-template "{path} {line} \[{msg_id}({symbol}), {obj}\] {msg}" libsan tests bin/sancli

regression:
  stage: Test
  needs:
    - linting
  script:
    - dnf install -y gcc python3-pip python3-augeas python3-netifaces python3-setuptools python3-wheel python3-devel findutils tox which rpm-build
    # Install libsan so all required dependencies are installed
    - python3 -m pip install .
    - cp sample_san_top.conf /etc/san_top.conf
    # Remove installation files
    - rm -rf build/ dist/ libsan.egg-info .eggs
    # Run tox tests
    - tox
  artifacts:
    paths:
      - htmlcov/

run:
  stage: Build
  needs:
    - regression
  script:
    - dnf install -y gcc python3-pip python3-augeas python3-netifaces python3-setuptools python3-wheel python3-devel findutils tox which rpm-build
    - python3 setup.py bdist_wheel
    # an alternative approach is to install and run:
    - python3 -m pip install dist/*
    # run the command here
  artifacts:
    paths:
      - dist/*.whl

# Moves pytest-cov html artifacts created in test stage to public pages
pages:
  needs:
    - regression
    - run
  stage: Deploy
  only:
    - master
  script:
    - mv htmlcov/ public/
    # - python3 -m pip install sphinx sphinx-rtd-theme
    # - cd doc ; make html
    # - mv build/html/ ../public/
  artifacts:
    paths:
      - public
